import java.util.*;
public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

    
        System.out.println();
        sc.close();
    }
}

一行一个数据
nextInt()

一行固定个数的数据
nextInt()

一行不定个数的数据
nextLine().split(" ")

搭配使用
nextInt() 和 hasNextInt()
nextLine() 和 hasNextLine()

输出答案时，注意格式
System.out.println();
System.out.print();

华为机考
Scanner 没有 close 报错
<>自动类型推断，报错